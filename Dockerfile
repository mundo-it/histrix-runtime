FROM php:7.4.1-apache-buster

LABEL maintainer="luis@mundoit.com.ar"

# Install Dependencies
ENV DEBIAN_FRONTEND noninteractive



# Configure TIMEZONE
RUN ln -fs /usr/share/zoneinfo/America/Argentina/Buenos_Aires /etc/localtime
RUN dpkg-reconfigure -f noninteractive tzdata

#Locales
ENV LANGUAGE="es_AR:es"
ENV LANG="es_AR.UTF-8"
ENV PHP_OPENSSL=yes

RUN apt-get update && apt-get install -y locales && \
    sed -i '/es_AR.UTF-8/s/^# //g' /etc/locale.gen && \
    locale-gen --no-purge $LANG

RUN apt-get update -y
RUN apt-get -y install gcc make autoconf libc-dev pkg-config libzip-dev


RUN apt-get install -y --no-install-recommends \
        git \
        libmemcached-dev \
        libz-dev \
        libpq-dev \
        libssl-dev libssl-doc libsasl2-dev \
        libmcrypt-dev \
        libxml2-dev \
        zlib1g-dev libicu-dev g++ \
        libldap2-dev libbz2-dev \
        curl libcurl4-openssl-dev \
        libgmp-dev firebird-dev libib-util \
        re2c libpng++-dev \
        libwebp-dev libjpeg-dev libjpeg62-turbo-dev libpng-dev libxpm-dev libvpx-dev libfreetype6-dev \
        libmagick++-dev \
        libmagickwand-dev \
        zlib1g-dev libgd-dev \
        libtidy-dev libxslt1-dev libmagic-dev libexif-dev file \
        sqlite3 libsqlite3-dev libxslt-dev \
        libmhash2 libmhash-dev libc-client-dev libkrb5-dev libssh2-1-dev \
        unzip libpcre3 libpcre3-dev \
        poppler-utils ghostscript libmagickwand-6.q16-dev libsnmp-dev libedit-dev libreadline6-dev libsodium-dev \
        freetds-bin freetds-dev freetds-common libct4 libsybdb5 tdsodbc libreadline-dev librecode-dev libpspell-dev libonig-dev

# fix for docker-php-ext-install pdo_dblib
# https://stackoverflow.com/questions/43617752/docker-php-and-freetds-cannot-find-freetds-in-know-installation-directories
RUN ln -s /usr/lib/x86_64-linux-gnu/libsybdb.so /usr/lib/

# RUN docker-php-ext-configure hash --with-mhash && \
# 	docker-php-ext-install hash
RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl && \
	docker-php-ext-install imap iconv

RUN docker-php-ext-install bcmath bz2 calendar ctype curl dba dom 
RUN docker-php-ext-install fileinfo exif ftp gettext gmp
RUN docker-php-ext-install intl json ldap mbstring mysqli
RUN docker-php-ext-install opcache pcntl pspell
RUN docker-php-ext-install pdo pdo_dblib pdo_mysql pdo_pgsql pdo_sqlite pgsql phar posix
#RUN docker-php-ext-install readline
RUN docker-php-ext-install session shmop simplexml soap sockets sodium
RUN docker-php-ext-install sysvmsg sysvsem sysvshm
# RUN docker-php-ext-install snmp

# fix for docker-php-ext-install xmlreader
# https://github.com/docker-library/php/issues/373
RUN export CFLAGS="-I/usr/src/php" && docker-php-ext-install xmlreader xmlwriter xml xmlrpc xsl

RUN docker-php-ext-install tidy tokenizer zend_test zip

# already build in... what they say...
# RUN docker-php-ext-install filter reflection spl standard
# RUN docker-php-ext-install pdo_firebird pdo_oci

# install pecl extension
RUN pecl install ds && \
	pecl install imagick && \
	pecl install igbinary && \
	pecl install memcached && \
	pecl install redis-5.1.0 && \
	pecl install mcrypt-1.0.3 && \
	docker-php-ext-enable ds imagick igbinary redis memcached

# https://serverpilot.io/docs/how-to-install-the-php-ssh2-extension
# 	pecl install ssh2-1.1.2 && \
# docker-php-ext-enable ssh2

# install pecl extension
RUN pecl install mongodb && docker-php-ext-enable mongodb

# install xdebug
# RUN pecl install xdebug && docker-php-ext-enable xdebug

RUN yes "" | pecl install msgpack && \
	docker-php-ext-enable msgpack

# install APCu
RUN pecl install apcu && \
	docker-php-ext-enable apcu --ini-name docker-php-ext-10-apcu.ini

RUN apt-get update -y && apt-get install -y apt-transport-https locales gnupg

# install MSSQL support and ODBC driver
# RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - && \
# 	curl https://packages.microsoft.com/config/debian/8/prod.list > /etc/apt/sources.list.d/mssql-release.list && \
# 	export DEBIAN_FRONTEND=noninteractive && apt-get update -y && \
# 	ACCEPT_EULA=Y apt-get install -y msodbcsql unixodbc-dev
# RUN set -xe \
# 	&& pecl install pdo_sqlsrv \
# 	&& docker-php-ext-enable pdo_sqlsrv \
# 	&& apt-get purge -y unixodbc-dev && apt-get autoremove -y && apt-get clean

# RUN docker-php-ext-configure spl && docker-php-ext-install spl

# install GD
RUN docker-php-ext-configure gd \
	#	--with-png \
	--with-jpeg \
	--with-xpm \
	--with-webp \
	--with-freetype \
	&& docker-php-ext-install -j$(nproc) gd

# set locale to utf-8
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
ENV LANG='en_US.UTF-8' LANGUAGE='en_US:en' LC_ALL='en_US.UTF-8'

#Runtime
RUN apt-get update && apt-get install -y \
    abiword \
    barcode \
    curl \
    dos2unix \
    rlpr \
    enscript \
    pdf2svg \
    ffmpeg \
    freetds-bin \
    freetds-dev \
    git \
    memcached \
    unzip \
    unixodbc-dev \
    net-tools \
    wget \
    zlib1g-dev \
    libbz2-dev \
    libc-client-dev \
    libcurl4-openssl-dev \
    libedit-dev \
    libfreetype6-dev \
    libicu-dev \
    libjpeg-dev \
    libjpeg62-turbo-dev \
    libkrb5-dev \
    libldap2-dev \
    libldb-dev \
    libmagickwand-dev \
    libmcrypt-dev \
    libmemcached-dev \
    libmemcachedutil2 \
    libonig-dev \
    libpng-dev \
    libpq-dev \
    libreadline-dev \
    libsasl2-dev \
    libsqlite3-dev \
    libssl-dev \
    libxml2-dev \
    libxslt1-dev \
    zip \
    freetds-common \
    libzip-dev \
    libzmq3-dev 
#
# Install PHP extensions and PECL modules
RUN docker-php-source extract \
#
# ODBC
&& cd /usr/src/php/ext/odbc \
&& phpize \
&& sed -ri 's@^ *test +"\$PHP_.*" *= *"no" *&& *PHP_.*=yes *$@#&@g' configure \
&& cd /usr/src/php/ext/odbc \
&& ./configure --with-unixODBC=shared,/usr \
&& docker-php-ext-install -j$(nproc) odbc && docker-php-ext-enable odbc \
#
# Standard Extensions
&& docker-php-ext-install -j$(nproc) bcmath bz2 calendar curl iconv intl mbstring mysqli opcache pdo_mysql pdo_pgsql pdo_sybase pdo_dblib pgsql soap zip exif xml xmlrpc \
&& apk add --no-cache zip libzip-dev \
&& docker-php-ext-configure zip  && docker-php-ext-install -j$(nproc) zip  \
#
# IMAP extension
&& export PHP_OPENSSL=yes; docker-php-ext-configure imap --with-kerberos --with-imap-ssl && docker-php-ext-install -j$(nproc) imap \
#
# ZMQ extension
&& cd /tmp && git clone --depth 1 https://github.com/zeromq/php-zmq && cd php-zmq && phpize && ./configure && make && make install \
&& docker-php-ext-enable zmq \
#
# GD module
&& docker-php-ext-configure gd --with-freetype --with-jpeg && docker-php-ext-install -j$(nproc) gd \
#
&& pecl install zip && docker-php-ext-enable zip \
#
# Ldap module
&& docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && docker-php-ext-install -j$(nproc) ldap \
#
# Sybase
#&& docker-php-ext-configure sybase_ct --with-sybase-ct=/usr && docker-php-ext-install -j$(nproc) sybase_ct \
#
# Igbinary module
#&& pecl install igbinary && docker-php-ext-enable igbinary \
#
# MEMCACHED module
#&& pecl install memcached && docker-php-ext-enable memcached \
#
# REDIS module
#&& pecl install redis && docker-php-ext-enable redis \
#
# Install Composer.
&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --1 --filename=composer \
&& ln -s $(composer config --global home) /root/composer \
#
# CLEAN
&& docker-php-source delete \
&& apt-get remove -y wget \
&& apt-get autoremove --purge -y \
&& apt-get autoclean -y \
&& apt-get clean -y \
&& rm -rf /var/lib/apt/lists/* \
&& rm -rf /tmp/* \
&& rm -rf /var/tmp/*


ENV PATH=$PATH:/root/composer/vendor/bin COMPOSER_ALLOW_SUPERUSER=1

# COPY histrix config
COPY ./apache-histrix.conf /etc/apache2/sites-available

# Enable Apache Modules
RUN a2enmod deflate \
&& a2enmod expires \
&& a2enmod rewrite \
&& a2enmod headers \
#
# Fix PHP.ini file
&& mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" \
&& sed -i '/error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT/c\error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE' $PHP_INI_DIR/php.ini \
&& sed -i '/memory_limit = 128M/c\memory_limit = 1G' $PHP_INI_DIR/php.ini \
#
# Disable default Apache website
&& a2dissite 000-default default-ssl \
#
# Enable histrix website
&& a2ensite apache-histrix

EXPOSE 80
